---
title: List
---

Mostly a personal notebook.

# Math and Physics

## Math

[Differential forms hypereggcrate](posts/2018-10-15-difffv.html)

[Cliff of Clifford algebra](posts/2018-10-23-clifalgbr.html)

[Groupie group](posts/2018-10-25-group.html)

[Monad, or Aion Teleos](posts/2019-04-20-monad.html)
[Monad and haskell](posts/2021-05-22-mohsk.html)

[Virosoro algebra](post/2021-04-22-viroal.html)

## Physics

[Spinor](posts/2018-10-20-spin1.html)
[Spinor2](posts/2020-07-19-spin2.html)

[Defected field](posts/2019-05-23-defqft.html)

# Music and other artsy stuff

## Ignisudo

*a noise generator with 4 guitarists, 3 physicists, 4 computer guys (only two of them can fix your computers, but the other two can fix your brains a bit), 3 teddy bears and a dragon.*

[Goldfish Memories](https://2er0.bandcamp.com/album/goldfish-memories)

[Tor10mm](https://2er0.bandcamp.com/track/tor10mm)

## 2ER0

*solo project based on my space soap opera sci-fi stories*

[Architecture of Echoes](https://2er0.bandcamp.com/album/architecture-of-echoes)

[Lump Homology](https://2er0.bandcamp.com/album/lump-homology)

[Arachnerella](https://2er0.bandcamp.com/album/arachnerella)

[Folk 1](https://2er0.bandcamp.com/album/folk-1)

## Jam

[Jam with Benji](https://2er0.bandcamp.com/album/jammbenj)

## Random Notes

[Readings](posts/2019-02-11-readings.html)

[Listenings](posts/2020-04-14-listenings.html)

[Üebunge](posts/2022-01-21-covers.html)

[Windsurf](posts/2023-07-01-windsurf.html)

# Swords

## HEMA

[Tempo](posts/2018-10-18-tempo.html)

## Koryu

# Computer stuff

[Hakyll](posts/2018-10-05-hakyll.html)

[Org-mode](posts/2018-10-10-orgmodetest.html)
