---
title: "Groupie group"
category: maphy
tags: math, algebra, physik
---


\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}

\usepackage[framemethod=TikZ]{mdframed}
\usepackage[cm]{sfmath}
\usepackage{amsfonts}
\usepackage{amsthm}

\usepackage{tabu}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{tikz}
\usepackage{bm}
\usepackage{xcolor}
\newmdtheoremenv[backgroundcolor=lightgray!20,linecolor=lightgray!50]{definition}{Definition}[chapter]%

\begin{document}

\newcommand{\iu}{\mathrm{i}}
\newcommand{\mref}[1]{\ref{#1} (Page \pageref{#1})}
\newcommand{\xrm}[1]{{#1}} % 3d-index in natural coordinate-basis
\newcommand{\xeu}[1]{\mathsf{#1}} % 3d-index in Cartan-basis (Euklidian)
\newcommand{\xal}[1]{\mathfrak{#1}} % index in Lie-algebra


\section{Groupie group}

\begin{mydef}
	A \textbf{Group} $(G, \cdot)$ is a set $G$ with a link $ \cdot : G \times G \rightarrow G $ with the following properties:

	\begin{itemize}
		\item \textbf{identity element}: \ \ $ \exists e \in G \ \forall g \in G : e \cdot g = g \cdot e = g $
		\item \textbf{inverse element}: \ \ $ \forall g \in G \ \exists g^{-1} \in G : g^{-1} \cdot g = g \cdot g^{-1} = e $
		\item \textbf{associativity}: \ \ $ \forall a,b,c \in G : (a \cdot b) \cdot c = a \cdot (b \cdot c) $
	\end{itemize}
\end{mydef}

An implication of this definition is that for any group element $g \in G$ the function
$$ L_g : G \rightarrow G : h \mapsto L_g(h) = g \cdot h $$
is bijective. It's called \textbf{left translation}.

\section{Lie group and algebra}

%\section{Lie groups and representations}

Many groups which are important to physics have continuous parameters, e.g. the rotations in 3-dimensional space ($SO(3)$) can be described by 3 angles, thus forming a 3-dimensional manifold.

\begin{mydef}
	A \textbf{Lie group} $(M, \phi)$ is an n-dimensional manifold $M$ with a join
	$$ \phi : M \times M \rightarrow M : (a,b) \mapsto \phi(a,b) =: a \cdot b \text{ ,} $$
	which satisfies the group axioms and is also smooth (continuously differentiable). If $M$ coordinates are chosen then $\phi$ can be written as n real functions ($ a,b,c \in M$ with coordinates $a^i, b^i, c^i, i = 1, \dots, n)$:
	$$ a \cdot b = c \ \Leftrightarrow \ \phi^i (a^1, \dots, a^n, b^1, \dots, b^n) = c^i \ . $$
\end{mydef}

Lie groups are often used to describe symmetries and transformations. Often, however, "infinitesimal" transformations are needed, ie a linearization of the Lie group around its identity element. Formally expressed:

\begin{mydef}
	The \textbf{Lie algebra} $\mathfrak{M}$ to a Lie group $M$ is the tangent space $T_eM$ of the identity element $e \in M$. This is an n-dimensional vector space whose basis vector $\{X_i\}$ are called \textbf{generators} of the group.

	On $\mathfrak{M}$ there is an additional $ [\cdot, \cdot] : \mathfrak{M} \times \mathfrak{M} \rightarrow \mathfrak{M} $ (\textbf{Lie bracket}), eg by:
	\begin{itemize}
		\item $ a,b \in \mathfrak{M} $
		\item Curves in the Lie group $ \gamma_a, \gamma_b : \mathbb{R} \rightarrow M $ by the identity element ($ \gamma_{a,b} (0) = e$) with tangent vectors $ \partial_t \gamma_{a,b}(t=0) = a,b $
		\item Lie bracket as a linearized commutator:
			$$ [a, b] := \partial_t \left(\gamma_a(t) \cdot \gamma_b(t) \cdot \gamma_a(t)^{-1} \cdot \gamma_b(t)^{-1}\right) (t=0) $$
	\end{itemize}
	This image is bilinear and antisymmetric. The development coefficients $c^k_{i,j}$ of the Lie bracket are called \textbf{structure constants}: $ [X_i, X_j] = c^k_{i,j} X_k $.
\end{mydef}

In many cases the group and algebra elements are given as matrices, so the Lie-bracket is the known commutator $ [a, b] = ab - ba $.

Two Lie algebras are isomorphic if they share the same structural constants. That is, if the structure constants are known, all properties of algebra follow from them. To some extent, even the Lie group can be reconstructed from them.

\subsubsection{Example}

The Lie groups $SU(2)$ and $SO(3)$ can each be considered 3-parameter rotations or 3-spheres. $SU(2)$ is the double overlay group to $SO(3)$, that is, $SO(3)$ can be embedded in the $SU(2)$, but there are closed paths in $SO(3)$, which must be traversed twice to return to the starting point in the $SU(2)$. A well-known example of this is the electron spin, which rotates at $360^\circ$, causing a sign change and returning to the original wave function at $720^\circ$.

Both groups are isomorphic in "not too big" domains, thus having the same Lie algebra. In the case of the $SU(2)$ form $\{X_i:=\iu\sigma_i\}$ with the Pauli matrices

\begin{equation*}
	\sigma_1 =
		\begin{pmatrix}
			0 & 1\\
			1 & 0
		 \end{pmatrix}, \ \
	\sigma_2 =
		\begin{pmatrix}
			0 & -\iu\\
			\iu & 0
		 \end{pmatrix}, \ \
	\sigma_3 =
		\begin{pmatrix}
			1 & 0\\
			0 & -1
		 \end{pmatrix}
\end{equation*}
The basis of the algebra $\mathfrak{su}(2)$. The structure consesult from the relationshiptants r $ \sigma_i \, \sigma_j = \delta_{ij} + \iu \, \varepsilon^{ijk} \, \sigma_k $ of the Paui matrices:
$$ c^k_{i,j} = - 2 \, \varepsilon^{ijk} \ . $$





\end{document}