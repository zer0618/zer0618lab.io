---
title: "Differential Forms hypereggcrate"
category: maphy
tags: math, diff-forms
---

\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}

\usepackage[framemethod=TikZ]{mdframed}
\usepackage[cm]{sfmath}
\usepackage{amsfonts}
\usepackage{amsthm}

\usepackage{tabu}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{tikz}
\usepackage{bm}
\usepackage{xcolor}
\newmdtheoremenv[backgroundcolor=lightgray!20,linecolor=lightgray!50]{definition}{Definition}[chapter]%

\begin{document}

\newcommand{\iu}{\mathrm{i}}
\newcommand{\mref}[1]{\ref{#1} (Page \pageref{#1})}
\newcommand{\xrm}[1]{{#1}} % 3d-index in natural coordinate-basis
\newcommand{\xeu}[1]{\mathsf{#1}} % 3d-index in Cartan-basis (Euklidian)
\newcommand{\xal}[1]{\mathfrak{#1}} % index in Lie-algebra


\section{Differential Forms Hypereggcrate}

\textbf{Differential forms} are:

(analytically) completely antisymmetric tensors;

(pictorially) interesting stacks of surface.

More details can be seen in Gravitation the bible.

\section{Definition}

The de Broglie wave has the most direct possible physical significance, diffract this wave from a crystal lattice, from the pattern of diffraction, one can determine not merely the length of the de Broglie waves, but also the pattern in space made by surfaces of equal, integral phase. This pattern of surfaces, provides the simplest illustration one can easily find for a 1-form.

\begin{definition}
		For a manifold $M$, the \textbf{differential forms} covariant tensor fields, which are antisymmetric in all parameters. The k-th order tensors are called k-forms. The set of k-forms is $\bigwedge^k(M)$, the set of all differential dorms $\bigwedge(M)$.

	According to the tensor product $\otimes$ there is the \textbf{outer product}

	$ \wedge : \bigwedge^k(M) \times \bigwedge^l(M) \rightarrow \bigwedge^{k+l}(M) : (\omega, \sigma) \mapsto \omega \wedge \sigma \ . $

	one can set $ \bigwedge^1(M) := \mathfrak{X}^\star(M) $ (dual vector fields) uad $ \bigwedge^0(M) := \mathfrak{F}(M) $ (scalar fields).
\end{definition}

The outer product has the following properties ($ \omega^i $ are dual vector fields, also 1-Forms):

\begin{itemize}
	\item $ \omega^i \wedge \omega^j = - \omega^j \wedge \omega^i $
	\item $ \omega^1 \wedge \dots \wedge \omega^k = 0 $, at all points where the $ \omega^1, \dots, \omega^k $ are not linearly independent
	\item implies that $ \bigwedge^k(M) = 0 $ for all $ k \geq \dim(M) $
	\item if $ \alpha \in \bigwedge^a(M), \beta \in \bigwedge^b(M) $, then: $ \alpha \wedge \beta = (-1)^{ab} \beta \wedge \alpha $
\end{itemize}

Inserting vector fields $X^i$ yields:

\begin{itemize}
	\item $ (\omega^1 \wedge \omega^2) (X^1, X^2) = \omega^1(X^1) \omega^2(X^2) - \omega^1(X^2) \omega^2(X^1) $
	\item in general: $ \alpha \in \bigwedge^a(M), \beta \in \bigwedge^b(M) $, then
		\begin{align}
			& (\alpha \wedge \beta) (X^1, \dots, X^{a+b}) \nonumber\\
			& = \sum_{\sigma \in S'_{a+b}} \operatorname{sign}(\sigma) \ \alpha(X^{\sigma(1)}, \dots, X^{\sigma(a)}) \ \beta(X^{\sigma(a+1)}, \dots, X^{\sigma(a+b)} ) \nonumber\\
			& = \frac{1}{ a! \ b! } \sum_{\sigma \in S_{a+b}} \operatorname{sign}(\sigma) \ \alpha(X^{\sigma(1)}, \dots, X^{\sigma(a)}) \ \beta(X^{\sigma(a+1)}, \dots, X^{\sigma(a+b)} )
			\label{eq:df_einsetzen}
		\end{align}
		($S_{a+b}$ are the permutations of $a+b$ elements, $S'_{a+b}$ are the permutations for which: $ \sigma(1) < \dots < \sigma(a) $ und $ \sigma(a+1) < \dots < \sigma(a+b) $)
\end{itemize}

\subsubsection{Basis}

As with the covariant tensors defined in the last section, the $dx^\mu$ form basis fields by which the 1-forms can be developed. For higher forms, however, it should be noted that not every combination of outer products of the $dx^\mu$ is required, since e.g. $ dx^1 \wedge dx^2 = - dx^2 \wedge dx^1 $. Each permutation of the indices changes at most the sign. Thus one can limit oneself to basis products, whose indices grow monotonously:

\begin{align}
 dx^\mu \wedge dx^\nu \wedge \dots \ \qquad \mu < \nu < \dots \quad . 
\end{align}

As with the tensors, the components of the differential forms result from the insertion of the basis vectors:

\begin{align}
 \omega \in \bigwedge^k(M) \ \Rightarrow \ \omega_{\mu_1, \dots, \mu_k} = \omega(\partial_{\mu_1}, \dots, \partial_{\mu_k}) \ . 
\end{align}

In order not to have to differentiate between sum convention for tensors and differential forms, in this work a notation is used, which sums \textbf{over all index combinations} and divides the superluous multiplicities out again:

\begin{align}
 \omega = \omega_{\alpha_1,\dots,\alpha_k} \, dx^{\alpha_1} \wedge \dots \wedge dx^{\alpha_k}, \ \ \alpha_1 < \dots < \alpha_k \ . 
\end{align}


\section{Hodge Duality}

The dimension of $M$ is n, then there are no k-forms with $k > n$. Likewise, there is only one n-form (except for multiplication with a scalar field), since there are just n linearly independent vectors in the tangent space. Generally there is $( n! )/( k! (n-k)! ) $ linearly independent k-form and thus as many as $(n-k)$-forms. It can be used to construct an isomorphism:

\begin{definition}
	The \textbf{Hodge star operator}

	$ \star : \bigwedge^k(M) \rightarrow \bigwedge^{n-k}(M) : \omega \mapsto \star \omega $

	be defined here by its effect on the components:

	\begin{align}
		\star \omega_{\mu_1,\ldots,\mu_{n-k}}
			& = \frac{1}{k!} \sqrt{|\det g|} \, \varepsilon_{\alpha_1,\ldots,\alpha_k,\mu_1,\ldots,\mu_{n-k}} \; \omega^{\alpha_1,\ldots,\alpha_k} \nonumber \\
			& = \frac{1}{k!} \sqrt{|\det g|} \, \varepsilon_{\alpha_1,\ldots,\alpha_k,\mu_1,\ldots,\mu_{n-k}} \; g^{\alpha_1,\beta_1} \dots g^{\alpha_k,\beta_k} \, \omega_{\beta_1, \ldots,\beta_k} \ .
		\label{eq:qp_df_dualcomp}
	\end{align}
\end{definition}

\subsubsection{Example}

$ M = \mathbb{R}^3 $ with the standard dual basis $ \{ dx^1, dx^2, dx^3 \} $, so that:

\begin{align*}
	\star 1 & = dx^1 \wedge dx^2 \wedge dx^3\\
	\star dx^1 & = dx^2 \wedge dx^3\\
	\star dx^2 & = dx^3 \wedge dx^1\\
	\star dx^3 & = dx^1 \wedge dx^2\\
	\star (dx^1 \wedge dx^2) & = dx^3 \ \ \\%%\text{(cyclic)}\\
	\star (dx^1 \wedge dx^2 \wedge dx^3) & = 1 \ .\\
\end{align*}

The Hodge star operator is often used in physics since together with the outer derivative defined in the next section, it allows one to elegantly formulate vector analysis operations (e.g. in electrodynamics ).



\section{Exterior derivative}

\begin{definition}
	The \textbf{Exterior derivative or outer derivative}

	$ d : \bigwedge^k(M) \rightarrow \bigwedge^{k+1}(M) : \omega \mapsto d\omega $

	is defined by:

	\begin{itemize}
		\item $ f \in \mathfrak{F}(M) = \bigwedge^0(M) \ \Rightarrow \ df = (\partial_\alpha f) dx^\alpha $
		\item $ \alpha \in \bigwedge^a(M), \beta \in \bigwedge^b(M) \ \Rightarrow \ d(\alpha \wedge \beta) = d\alpha \wedge \beta + (-1)^a \alpha \wedge d\beta $
		\item $ dd\omega = 0 \ \forall \omega \in \bigwedge(M) $
	\end{itemize}
\end{definition}

In coordinates applies

\begin{align*}
	d\omega = \frac{1}{ k! } d\omega_{\alpha_1, \dots, \alpha_k} \wedge dx^{\alpha_1} \wedge \dots \wedge dx^{\alpha_k}
	= \frac{1}{ k! } (\partial_\beta \, \omega_{\alpha_1, \dots, \alpha_k}) \, dx^\beta \wedge dx^{\alpha_1} \wedge \dots \wedge dx^{\alpha_k} \ .
\end{align*}

The outer $dx^\mu$ also justifies the notation $dx^\mu$ for the dual basis vectors. They are simply the 1-form that arise when outer derivative affects the individual components of the map function: $d(x^\mu)$.

When generalizing the derivative to \textbf{vector-valued} functions and forms, the individual vector components are interpreted and derived as scalar functions or forms. Is e.g. $V$ a vector space with basis $\{\textbf{e}_i\}$ and $f$ a function $ f : M \rightarrow V $, then:

\begin{align}
 df = d(f^a  \textbf{e}_a) = (d f^a) \textbf{e}_a = (\partial_\beta f^a)  dx^\beta  \textbf{e}_a  
\end{align}

This is a vector of 1-forms ("vector-valued 1-form"). Note that $V$ does not necessarily have to be the tangent space. For this work, mainly forms with values of Lie algebras are important.

\subsubsection{Duality plus exterior derivative}

Start with scalar $\phi$, Its gradient $d\phi$ is a 1-form. Take its dual, to get the 3-form $\star d \phi$. Take its exterior derivative, to get the 4-form $d \star d \phi$. Take its dual, to get the scalar $ \Box \phi = - \star d \star d \phi$. Verify by index manipulations that $\Box$ as defined here is the wave operator; i.e. in any Lorentz frame, $\Box \phi = {\phi_{,\alpha}}^{,\alpha} = - (\partial ^2 \phi / \partial t^2) + \nabla ^2 \phi$.

Star with 1-form $A$. Get 2-form $F=dA$. Take its dual $\star F = \star d A$, also a 2-form. Take its exterior derivative, obtaining the 3-form $d \star F$ (has value $4\pi \star J$ in electromagnetism). Take its dual, obtaining the 1-form $\star d \star F = \star d \star d A = 4 \pi J $ ("Wave equation for electromagnetic 4-potential"). 



\section{Simple examples}

\subsubsection{Maxwell's equations in the language of differential forms}

The general 2-forms \textbf{F} is written as a superposition of wedge products with a factor 1/2.

%\todo{maybe change all the normal d to \textbf{d} to make it more clear as outer derivative}

\begin{align}
		\textbf{F} = \frac12 F_{\mu\nu} dx^\mu \wedge dx^\nu
\end{align}

( $F_{xy}dx \wedge dy$ $F_{yx}dy \wedge dx $ with $F_{yx}=-F{xy}, dy \wedge dx = - dx \wedge dy $ appears twice)

If the differentiation ("taking the gradient", the operator \textbf{d}) produced out of a scalar a 1-form, it is also true that differentiation (again the operator \textbf{d}, but now generally known under Cartan's name of "exterior differentiation") produces a 2-form out of the general 1-form. The general \textit{f}-form as

\begin{align}
		\Phi = \frac{1}{\textit{f}!} \phi_{\alpha_1 \alpha_2 ...\alpha_{\textit{f}}} dx^{\alpha_1} \wedge dx^{\alpha_2} \wedge ... \wedge dx^{\alpha_{\textit{f}}}
\end{align}

Take the exterior derivative

\begin{align*}
		d \textbf{F} =& (\frac{\partial B_x}{\partial x} + \frac{\partial B_y}{\partial y} + \frac{\partial B_z}{\partial z})dx \wedge dy \wedge dz \\
					  & + (\frac{\partial B_x}{\partial t} + \frac{\partial E_z}{\partial y} - \frac{\partial E_y}{\partial z}) dt \wedge dy \wedge dz\\
					  & + (\frac{\partial B_y}{\partial t} + \frac{\partial E_x}{\partial z} - \frac{\partial E_z}{\partial x}) dt \wedge dz \wedge dx\\
					  & + (\frac{\partial B_z}{\partial t} + \frac{\partial E_y}{\partial x} - \frac{\partial E_x}{\partial y}) dt \wedge dx \wedge dy
\end{align*}

Each term in this expression is familiar from Maxwell's equations

\begin{align*}
 div B = \nabla \cdot B = 0 
\end{align*}

and 

$$ curl E = \nabla \times E = - \dot{B} $$

Each vanishes

\begin{align*}
		d \textbf{F} = 0
		\label{eq:a_f}
\end{align*}

it is a closed 2-form, too. The density of endings is described by the 3-form $\star \textbf{J} = \textbf{charge}$.

The two equations \eqref{eq:a_f} and \eqref{eq:a_m} summarize the entire content of Maxwell's equations in geometric language.

\subsubsection{Gravitational flux}

Place a "point particle" of mass $m$, then this generates a force on any particle of unit mass at $\textbf{r}=(x, y z)$ given by

\begin{align}
 \textbf{F} = \frac{-m \textbf{r}}{r^3} 
\end{align}

The corresponding 2-form is given by

\begin{align}
 \omega = -m \frac{xdy \wedge dz + ydz \wedge dx + zdx \wedge dy}{r^3} 
\end{align}

work in spherical coordinates

\begin{align}
 \omega = -m sin(\phi) d\phi \wedge \theta 
\end{align}

Let $S_R$ be the boundary of the ball of radius $R$ around 0. 

\begin{align}
 \int \int_{S_R} \textbf{F} \cdot \frac{\textbf{r}}{r} dS = \int \int_{S_R} \omega = - \int_0^{2\pi} \int_0^\pi d \phi d \theta = 4 \pi m 
\end{align}

Can be seen that $d\omega=0$, Therefore the Gauss' theorem yields

\begin{align}
 \int \int_S \omega = \int \int \int_V d \omega = 0 
\end{align}

if the interior $V$ does not contain 0. On the other hand, if $V$ contains 0, let $B_R$ be a small ball contained in $V$, and let $V-B_R$ donate part of $V$ lying outside of $B_R$.

\begin{align}
 \int \int_S \omega - \int \int_{S_R} \omega = \int \int \int _{V-B_R} d \omega = 0 
\end{align}

subtracting the second surface integral

\begin{align}
 \int \int_S \omega = - 4\pi m 
\end{align}

It can be easily extracted an expression for the flux for several particles or even a continuous distribution of matter. If $\textbf{F}$ is the force of gravity associated with some mass distribution, for any closed surface $S$ oriented by the outer normal, then the flux

\begin{align}
 \int \int_S \textbf{F} \cdot \frac{\textbf{r}}{r} dS 
\end{align}

is $-4\pi$ times the mass inside S. For a continuous distribution with density $\mu$

\begin{align}
 \int \int \int_V (\nabla \cdot \textbf{F} + 4 \pi \mu) dV =0 
\end{align}

for all regions $V$. Therefore

\begin{align}
		\nabla \cdot \textbf{F} = - 4 \pi \mu
\end{align}



\end{document}
