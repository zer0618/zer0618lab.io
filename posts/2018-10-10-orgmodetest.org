---
title: "org-mode test"
tags: computerstuff, emacs
---

#+AUTHOR: 2ER0
#+KEYWORDS: emacs, org-mode
#+OPTIONS: H:4 toc:t



* org-mode
** header 2
writing on the outliner
** it's nice and fast
* can export as other format
supporting html, pdf and more

#+BEGIN_SRC emacs-lisp
* This is H1
There is paragraph under h1
** H2
*** H3
* Some basic test
This is *bold*, /italic/, =code=, ~verbatim~ and +strike+ text.
#+END_SRC

#+BEGIN_SRC emacs-lisp
* List
- Bullet
- Another bullet
  - bug
    - red

** Other style
+ Bullet
+ Another bullet
  * bug
    * red

** Other style
1. Bullet
2. Another bullet
   1) bug
      1. red
#+END_SRC

#+BEGIN_QUOTE
lalala
#+END_QUOTE

#+BEGIN_CENTER
lululu\\
lala
#+END_CENTER

* Tables

#+BEGIN_SRC emacs-lisp
|buy        |number|for
|-----------+------+---------|
|Knoblauch  |3     |Dinner   |
|Milch      |1     |Frühstück|
#+END_SRC

|buy        |number|for
|-----------+------+---------|
|Knoblauch  |3     |Dinner   |
|Milch      |1     |Frühstück|

after typing "|buy|number|for", can press =C-c= =RET= to make the table automaticly.

* Source Code

*Emacs Lisp:*

#+BEGIN_EXAMPLE
#+BEGIN_SRC emacs-lisp
(defun negate (x)
    "Negate the value of x."
    (- x))
#+END_SRC
#+END_EXAMPLE

#+BEGIN_SRC emacs-lisp
(defun negate (x)
    "Negate the value of x."
    (- x))
#+END_SRC

#+BEGIN_EXAMPLE
#+BEGIN_SRC emacs-lisp :results output
(print
    (negate 10))
#+END_SRC

#+RESULTS:
:
: -10
#+END_SRC
#+END_EXAMPLE

#+BEGIN_SRC emacs-lisp :results output
(print
    (negate 10))
#+END_SRC

#+RESULTS:
:
: -10

*Haskell:*

#+BEGIN_SRC haskell :results output
factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n - 1)
#+END_SRC

* LaTeX

- *Letters:* \alpha \beta \rightarrow \uparrow \or \and \implies \pi \infty
- *Inline Math:* $f(x) = x^2$

** \mathscr{Hello!}

\begin{equation}
x=\sqrt{b}
\end{equation}

*NOTE:* using [[MathJax][https://www.mathjax.org/]] /instead of the standard LaTeX which is skipped during compilation to HTML./

* Exporting

https://orgmode.org/manual/Exporting.html

** Settings
Include at the beginning of the file 

#+BEGIN_SRC emacs-lisp
#+TITLE:       the title to be shown (default is the buffer name)
#+AUTHOR:      the author (default taken from user-full-name)
#+DATE:        a date, an Org timestamp1, or a format string for format-time-string
#+EMAIL:       his/her email address (default from user-mail-address)
#+DESCRIPTION: the page description, e.g. for the XHTML meta tag
#+KEYWORDS:    the page keywords, e.g. for the XHTML meta tag
#+LANGUAGE:    language for HTML, e.g. ‘en’ (org-export-default-language)
#+TEXT:        Some descriptive text to be inserted at the beginning.
#+TEXT:        Several lines may be given.
#+OPTIONS:     H:2 num:t toc:t \n:nil @:t ::t |:t ^:t f:t TeX:t ...
#+BIND:        lisp-var lisp-val, e.g.: org-export-latex-low-levels itemize
               You need to confirm using these, or configure org-export-allow-BIND
#+LINK_UP:     the ``up'' link of an exported page
#+LINK_HOME:   the ``home'' link of an exported page
#+LATEX_HEADER: extra line(s) for the LaTeX header, like \usepackage{xyz}
#+EXPORT_SELECT_TAGS:   Tags that select a tree for export
#+EXPORT_EXCLUDE_TAGS:  Tags that exclude a tree from export
#+XSLT:        the XSLT stylesheet used by DocBook exporter to generate FO file
#+END_SRC

the =#+OPTION= includes:

#+BEGIN_SRC emacs-lisp
H:         set the number of headline levels for export
num:       turn on/off section-numbers
toc:       turn on/off table of contents, or set level limit (integer)
\n:        turn on/off line-break-preservation (DOES NOT WORK)
@:         turn on/off quoted HTML tags
::         turn on/off fixed-width sections
|:         turn on/off tables
^:         turn on/off TeX-like syntax for sub- and superscripts.  If
           you write "^:{}", a_{b} will be interpreted, but
           the simple a_b will be left as it is.
-:         turn on/off conversion of special strings.
f:         turn on/off footnotes like this[1].
todo:      turn on/off inclusion of TODO keywords into exported text
tasks:     turn on/off inclusion of tasks (TODO items), can be nil to remove
           all tasks, todo to remove DONE tasks, or list of kwds to keep
pri:       turn on/off priority cookies
tags:      turn on/off inclusion of tags, may also be not-in-toc
<:         turn on/off inclusion of any time/date stamps like DEADLINES
*:         turn on/off emphasized text (bold, italic, underlined)
TeX:       turn on/off simple TeX macros in plain text
LaTeX:     configure export of LaTeX fragments.  Default auto
skip:      turn on/off skipping the text before the first heading
author:    turn on/off inclusion of author name/email into exported file
email:     turn on/off inclusion of author email into exported file
creator:   turn on/off inclusion of creator info into exported file
timestamp: turn on/off inclusion creation time into exported file
d:         turn on/off inclusion of drawers
#+END_SRC

for example, this file includes:

#+BEGIN_SRC emacs-lisp
#+TITLE: org-mode test
#+AUTHOR:2ER0
#+EMAIL: whatever@somewhere.com
#+KEYWORDS: computerstuff, org-mode
#+OPTIONS: H:4 toc:t 
#+END_SRC

** Include files

#+BEGIN_SRC emacs-lisp
  #+INCLUDE: "~/folder/file" src emacs-lisp
#+END_SRC

** Export

use 

#+BEGIN_SRC emacs-lisp
  C-c C-e
#+END_SRC

and choose the file format, simple and fast.